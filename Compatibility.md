This project has been tested successfully on :
* Google Chome 18, 21
* Firefox 12, 14
* Safari 5.1.7

_And it should work fine on the other browsers._

You can really help by testing it and sending to me feedbacks.
