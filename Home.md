Welcome to the QtWebsocket wiki!

A Qt Websocket server.

* [How to install](https://github.com/ant-lafarge/QtWebsocket/wiki/Install)
* [Functionnalities](https://github.com/ant-lafarge/QtWebsocket/wiki/Functionnalities)
* [Compatibility](https://github.com/ant-lafarge/QtWebsocket/wiki/Compatibility)
* [Todo list](https://github.com/ant-lafarge/QtWebsocket/wiki/Todo)
* [Websocket drafts](https://github.com/ant-lafarge/QtWebsocket/wiki/Websocket-drafts)
* [QtWebsocket-for-Qt4](https://github.com/ant-lafarge/QtWebsocket/wiki/Qt4)
* [Contribution information](https://github.com/ant-lafarge/QtWebsocket/wiki/Contributions)

Thanks for your interest.