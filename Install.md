For now, you don’t need to compile it.

You just have to add these files directly in your project :
* QWsServer.h
* QWsServer.cpp
* QWsSocket.h
* QWsSocket.cpp

The current way to use the lib is to create your own server class (like the serverExample class).
Normaly, you don’t have to modify the last enumered files, unless there is a bug.
But in this last case, it is best to notice me for a future fix !
