Implemented functionnalities:
* Opening handshake
* Closing handshake
* Frames (send and receive)
* Mask sent frames, receive masked frames
* Control frames (ping, pong)
* Differents payload Lengths
* Multi-frames (send and receive)
* Support for websocket protocol version 0, 8 and 13
* QWsSockets can be moved in other threads (cf. ServerExampleThreaded)